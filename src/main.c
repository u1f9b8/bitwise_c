/*
 * src/main.c
 * Código criado com o objetivo de testar a performance dos operadores bitwise
 * e módulo, em meio a estruturas de condições (if-else e ternário).
 *
 * Para fins de análise, o programa gera um número inteiro randomizado
 * de 1 a 1000 e, cada função verifica e retorna o valor "ímpar" ou "par".
 *
 * A performance de cada função é calculada individualmente.
 *
 * Durante a execução do programa, a função bitwise_ternary apresentou
 * uma leve perda de performance, em comaparação as outras.
 *
 * O código foi compilado com GCC na versão 14.1.1.
 *
 * by @divisplima.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_RANDOM 1000

const char* bitwise_ternary(int n) {
	return (n & 1) ? "ímpar" : "par";
}

const char* bitwise_if_else(int n) {
	if (n & 1)
		return "ímpar";
	else
		return "par";
}

const char* module_ternary(int n) {
	return (n % 2 == 0) ? "par" : "ímpar";
}

const char* module_if_else(int n) {
	if (n % 2 == 0)
		return "par";
	else
		return "ímpar";
}

int main(int argc, char *argv[]) {
	clock_t start, end;
	double cpu_time_used;
	const char* result;
	int n;

	/* Gerador de números inteiros randomizados */
	srand(time(NULL));
	n = rand() % (MAX_RANDOM + 1);
	printf("Número gerado: %d.\n\n", n);

	/* bitwise + ternário */
	start = clock();
	result = bitwise_ternary(n);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("bitwise_ternary: %d é %s.\nTempo: %fs.\n\n",n, result, cpu_time_used);

	/* bitwise + if-else */
	start = clock();
	result = bitwise_if_else(n);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("bitwise_if_else: %d é %s.\nTempo: %fs.\n\n", n, result, cpu_time_used);

	/* módulo + ternário */
	start = clock();
	result = module_ternary(n);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("module_ternary: %d é %s.\nTempo: %fs.\n\n", n, result, cpu_time_used);

	/* módulo + if-else */
	start = clock();
	result = module_if_else(n);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("module_if_else: %d é %s.\nTempo: %fs.\n", n, result, cpu_time_used);

	return EXIT_SUCCESS;
}

